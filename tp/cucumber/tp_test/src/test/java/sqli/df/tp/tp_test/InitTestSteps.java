package sqli.df.tp.tp_test;

import org.junit.Assert;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class InitTestSteps {
	private InitTest initTest;
	private int le_juste;
	private int voulant_entrer;
	private int prix;
	private Person leBlanc = new Person("M.le blanc");
	private Person Pignon = new Person("M.pignons");
	
	@Given("^I want to configure my firefox browser$")
	public void i_want_to_configure_my_firefow_browser() throws Throwable {
		initTest = new InitTest();
		initTest.configureFirefoxBrowser();
	}
	
	@When("^I navigate to Google$")
	public void i_navigate_to_google() throws Throwable {
		initTest.navigateToGoogle();
	}
	
	@Then("^the title of the web page is Google$")
	public void the_title_of_the_web_page_is_google() throws Throwable {
		Assert.assertTrue("Title does match", initTest.getDriver().getTitle().equals("Google"));
	}
	
	@And("^I want to close my browser$")
	public void i_want_to_close_my_browser() throws Throwable {
		initTest.closeBrowser();
	}
	
	@Given("^(\\d+) et (\\d+) veulent rentrer dans le bar$")
	public void et_veulent_rentrer_dans_le_bar(int arg1, int arg2) throws Throwable {
	     voulant_entrer = arg1 + arg2;

	}

	@When("^(\\d+) à (\\d+) en son sein$")
	public void à_en_son_sein(int arg1, int arg2) throws Throwable {
	    le_juste = arg1 - arg2;
	}

	@Then("^(\\d+) affiche complet$")
	public void affiche_complet(int arg1) throws Throwable {
	   if (le_juste >= voulant_entrer) {
		   le_juste = le_juste - voulant_entrer;
	   } else {
		   System.out.println("vous n'entrez pas, c'est complet");
	   }
	   if (le_juste == 0) {
		   System.out.println("c'est complet");
	   }
	}

	@Then("^(\\d+) affiche bienvenu$")
	public void affiche_bienvenu(int arg1) throws Throwable {
		if (le_juste <= voulant_entrer) {
			   le_juste = le_juste - voulant_entrer;
			   System.out.println("entrez");
		   } 
		if (le_juste == 0) {
			   System.out.println("c'est complet");
		   }
	}
	@Given("^<M\\.pignon> et <M\\.le blanc> veulent rentrer dans le bar$")
	public void m_pignon_et_M_le_blanc_veulent_rentrer_dans_le_bar() throws Throwable {
	    voulant_entrer = leBlanc.getRepresente() + Pignon.getRepresente();
	    affiche_bienvenu( voulant_entrer);
	    
	}

	@When("^(\\d+) veut entrer$")
	public void veut_entrer(String arg1) throws Throwable {
	     Person people = new Person(arg1);
	     voulant_entrer = people.getRepresente();
	}

	@Then("^ils commandent un coktail à (\\d+)$")
	public void ils_commandent_un_coktail_à(int arg1) throws Throwable {
	     int group = leBlanc.getRepresente() + Pignon.getRepresente();
	     prix = group * arg1;
	     System.out.println("le prix total est de :" + prix);
	     
	}

	@Then("^(\\d+) paye\\.$")
	public void paye(int arg1) throws Throwable {
	    //foreach d'un tableau de personnes et on compare le person.prenom si il est égale à arg1
		// si il est égal alors payeur = this.person
		// du coup on setnotebar (prix) de this.person
	}

	@Then("^(\\d+) est heureux car ils n ont bu que (\\d+) verre$")
	public void est_heureux_car_ils_n_ont_bu_que_verre(String arg1, int arg2) throws Throwable {
	    // on cherche le arg1 dans un tableau de personne
		// si arg2 < 2 alors this.person.sentiment == enum.heureux
	}

}
