package sqli.df.tp.tp_test;

public class Person {
	private String prenom;
	private int verreConsommes;
	private int represente;
	private double noteBar;
	private Humeur sentiments;
	
	private enum Humeur{
		heureux,
		neutre,
		triste
	}
	
	public Person(String p1) {
		this.prenom = p1;
		this.setRepresente(1);
		this.setVerreConsommes(0);
		this.setSentiments(Humeur.neutre);
		this.setNoteBar(0);
	}

	public double getNoteBar() {
		return noteBar;
	}

	public void setNoteBar(double noteBar) {
		this.noteBar = noteBar;
	}

	public int getVerreConsommes() {
		return verreConsommes;
	}

	public void setVerreConsommes(int verreConsommes) {
		this.verreConsommes = verreConsommes;
	}

	public Humeur getSentiments() {
		return sentiments;
	}

	public void setSentiments(Humeur sentiments) {
		this.sentiments = sentiments;
	}

	public int getRepresente() {
		return represente;
	}

	public void setRepresente(int represente) {
		this.represente = represente;
	}
	
	
}
