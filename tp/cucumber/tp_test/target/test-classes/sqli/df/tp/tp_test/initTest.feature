#Author: dfrances@sqli.com
Feature: Init test

  Scenario: Init test
    Given I want to configure my firefox browser
    When I navigate to Google
    Then the title of the web page is Google
    And I want to close my browser

    Scenario Outline: 

    Given <pignon> et <le blanc> veulent rentrer dans le bar
    When <le juste> à <client> en son sein
    Then <le juste> affiche <resultat>
    
    Examples:
     | pignon | le blanc | le juste | client | resultat |
     | 1      | 1        | 10       | 9      | complet  |
     | 1      | 1        | 10       | 8      | bienvenu |
     | 1      | 1        | 10       | 7      | bienvenu |
     
    Scenario Outline:
     
    Given <M.pignon> et <M.le blanc> veulent rentrer dans le bar
    When <le juste> à <client> en son sein
    When <another client> veut entrer
    Then <le juste> affiche <resultat>
    And ils commandent un coktail à <prix coktail>
    And  <M.le blanc> paye.
    And <pignon> est heureux car ils n ont bu que <bu> verre
    
 Examples:
     | M.pignon | M.le blanc | le juste | client | resultat | another client | prix coktail | bu |
     | M.pignon | M.le blanc | 10       | 9      | complet  | M.X            | 10           | 1  |
     | M.pignon | M.le blanc | 10       | 8      | bienvenu | M.X            | 15           | 3  |
     | M.pignon | M.le blanc | 10       | 7      | bienvenu | M.X            | 5            | 5  |


