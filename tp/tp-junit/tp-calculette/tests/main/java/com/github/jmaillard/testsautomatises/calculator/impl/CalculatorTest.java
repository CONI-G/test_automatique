package main.java.com.github.jmaillard.testsautomatises.calculator.impl;

import static org.junit.Assert.*;

import org.junit.Test;

public class CalculatorTest {

	@Test
	public void I_should_get_5when_I_add_2_to_3() {
		//GIVEN
	 Calculator c1 = new Calculator();
	 //WHEN
	 
	 assertEquals(c1.sum(2, 3),5);
	}

	@Test
	public void I_should_get_minus1when_I_add_minus3_to_2() {
		//GIVEN
		 Calculator c1 = new Calculator();
		 //WHEN
		 
		 assertEquals(c1.sum(2, -3),-1);
	}

	@Test
	public void I_should_get_minus1_when_I_drop_minus3_to_2() {
		//GIVEN
		 Calculator c1 = new Calculator();
		 //WHEN
		 
		 assertEquals(c1.minus(2, -3),5);
	}

	@Test
	public void I_should_get_5_when_I_drop_minus3_to_2() {
		//GIVEN
		 Calculator c1 = new Calculator();
		 //WHEN
		 
		 assertEquals(c1.minus(2, -3),5);
	}
	@Test
	public void I_should_get_5_when_I_divide_10_by_2() {
		//GIVEN
		 Calculator c1 = new Calculator();
		 //WHEN
		 
		 assertEquals(c1.divide(10, 2),5);
	}
	
	@Test(expected = ArithmeticException.class)
	public void I_should_get_error_when_I_divide_10_by_0() {
		//GIVEN
		 Calculator c1 = new Calculator();
		 //WHEN
		 
		 c1.divide(10, 0);
	}
	@Test
	public void I_should_get_6_when_I_multiply_3_by_2() {
		//GIVEN
		 Calculator c1 = new Calculator();
		 //WHEN
		 
		 assertEquals(c1.multiply(3, 2),6);
	}
	@Test
	public void I_should_get_0_when_I_multiply_0_by_2() {
		//GIVEN
		 Calculator c1 = new Calculator();
		 //WHEN
		 
		 assertEquals(c1.multiply(0, 2),0);
	}


}
