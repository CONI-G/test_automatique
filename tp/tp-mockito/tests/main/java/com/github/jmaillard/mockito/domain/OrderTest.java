package main.java.com.github.jmaillard.mockito.domain;

import static org.junit.Assert.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;


@RunWith(MockitoJUnitRunner.class)
public class OrderTest {

	@Test
	public void should_command_get_9_99_with_2_product_one3_99_two_5_00() throws Exception{
		
		// GIVEN
		
        Product product1 = Mockito.mock(Product.class);
        Product product2 = Mockito.mock(Product.class);
        
        Order order = Mockito.spy(new Order(null));
        order.products.add(product1);
        order.products.add(product2);
        
        // WHEN

        Mockito.when(product1.getPrice()).thenReturn(new BigDecimal("3.99"));
        Mockito.when(product2.getPrice()).thenReturn(new BigDecimal("5.00"));

        // THEN
        Assert.assertEquals(new BigDecimal("8.99"), order.getTotalPrice());
	}
	
	@Test
	public void should_formatTotalPrice(){
		
	}
		// GIVEN
	
		Order order = Mockito.mock(Order.class);
		Locale locale = Mockito.spy(new Locale(null));
		//locale.orders.getTotalPrice();
	}


